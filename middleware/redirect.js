export default function ({ store, redirect }) {
  return redirect('/' + store.state.editions.length)
}
  