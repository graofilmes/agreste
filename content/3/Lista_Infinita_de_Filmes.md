---
order: 5
title: LISTA INFINITA DE FILMES BRASILEIROS SOBRE A LUTA POR MORADIA
author: Mirrah da Silva
img: 3/bgs/Sem-titulo1.jpg
alt: Fotografia de um prédio deadente na cidade
list: true
---

Passei os últimos anos assistindo tudo quanto é tipo de filme sobre a luta por direito à moradia, direito à cidade, gentrificação, especulação imobiliária, etc. Comecei a fazer uma lista pessoal com os links para que eu não me perdesse, hoje publico na AGRESTE. Segue abaixo a lista de filmes e links cujas produções estão disponíveis online. É importante falar que Dênia Cruz, Cristiano e eu, estamos no ímpeto de realizar a Mostra Moradia e reunir os filmes em uma plataforma única. Os filmes a seguir contemplam principalmente a disputa pelo território urbano. A lista a seguir é infinita e será atualizada de tempos em tempos:
