---
order: 11
title: Solidariedade Pandêmica
author: Vanessa Zattler
img: 3/bgs/Travesti-Artista-Resiste.jpg
alt: Fotografia de um prédio deadente na cidade
---

A ideia de mobilizar mais de 20 pessoas das mais diversas regiões do planeta a escreverem juntas um livro em abril deste ano, durante a escalada planetária da Covid-19, foi bem definida por Rebecca Solnit no prefácio do mesmo. Ela diz que redes de solidariedade e ajuda mútua não são “coisas que brotam de repente, como cogumelos depois de uma chuva, ou coisas que ocorrem apenas a partir dos esforços de pessoas extraordinárias. Elas sempre estiveram sendo cultivadas, por todos os lados, e são o que sustentam o movimento anti capitalista”.

Foi a partir desse pensamento que a socióloga norte americana Marina Sitrin convidou pessoas de diferentes regiões do mundo para trazer narrativas de lugares tão diferentes quanto Rojava, Itália, Taiwan, Iraque e Brasi,l entre outros. Desta forma, através de um trabalho coletivo e prefigurativo, pela forma horizontal na qual foi feito, nasceu o livro Pandemic Solidarity<vue-tooltip text="Mais informações em: <a href='https://pandemicsolidarity.net/' target='_blanc'>www.pandemicsolidarity.net</a><br />" index="1"></vue-tooltip>, assinado por Marina Sitrin e essa coletiva que passou a ser chamada Colectiva Sembrar.

Sembrar significa semear, em espanhol, e fala sobre as sementes e as germinações que ocorrem e se fortalecem através da solidariedade ao redor do mundo, e que parecem inclusive se fortalecer em momentos desastrosos como o que vivemos.

Marina Sitrin escreve que há algo profundo nisso que nos conecta com a verdade sobre quem realmente somos, e não a que somos ditos que somos. Sim, nós temos medo. Nós sentimos dor e vulnerabilidade, e o que nós fazemos com isso, de novo e de novo, ao longo da história e agora mais do que nunca, é procurarmos uns aos outros e encontrar maneiras de nos cuidarmos coletivamente.”

Eu escrevo a parte sobre o Brasil no livro, e aqui compartilho as entrevistas feitas<vue-tooltip text="Se você estiver interessada em ler o livro gratuitamente em PDF envie uma mensagem para o Instagram de Vanessa Zettler @vanessazet" index="2">2</vue-tooltip>.

São entrevistas com pessoas que têm um envolvimento contínuo com diferentes formas de organização de base. Negras e indígenas, gente nas periferias das grandes cidades, gente da terra, mulheres. Pessoas e locais nos quais a Covid-19 não é a principal ameaça aos seus modos de vida. O que levo ao livro é um recorte de uma área específica do Brasil; de áreas não centrais das cidades mais densas do país, São Paulo e Rio de Janeiro.

É uma mostra de como o trabalho dessas pessoas é fundamental para que um outro mundo possível exista. Ele já existe. Está nas brechas, sendo construído a gerações. Olhar para e viver nas brechas das possibilidades sempre se fez necessário.

<hr />

**Um bar no Campo Limpo, periferia de São Paulo, foi a base para o surgimento de um sarau semanal no ano de 2004, reunindo poetas, músicos e agitadores culturais locais. Suzi de Aguiar Soares, 53, e o marido Binho não têm mais o bar, mas o Sarau do Binho tornou-se uma referência para a literatura e as artes periféricas. Agora também se tornou um eixo de luta contra a COVID-19, sendo central na organização de uma grande rede de apoio mútuo.**

“Assim que o Covid-19 começou, houve uma grande angústia. Começamos então a colocar material na web. Nossa poesia e música, com a hashtag #SarauemTemposdeCorona, incentivou as pessoas a também enviarem vídeos, o que cresceu muito. Isso se tornou terapêutico, proporcionando a muitas pessoas espaço para se sentirem melhor. Acho que através da arte podemos curar algumas feridas, ajudar pessoas que não estão bem.

A poesia é uma forma de cura.

Também iniciamos uma arrecadação de fundos junto com outros coletivos locais na área.  Conseguimos ajudar mais de mil famílias da região, muito antes de chegar a ajuda do governo. Temos feito parte de uma rede muito grande, e isso só a faz crescer ainda mais.

Comecei ligando para as pessoas e verificando como estavam. Solidariedade é perguntar: como vai você? Como posso ajudar?

Existem pessoas que não expressam sua necessidade de ajuda. Tem gente que tá só deprimido... Uma amiga minha, por exemplo, escritora, com 4 filhos, me disse que não conseguia sair da cama. Ela havia recentemente parado de lecionar na escola para viver de sua escrita. E por isso ela não tinha perspectiva de trabalho e estava deitada na cama. Conversei com ela, mandei dinheiro e depois ela foi comprar comida, mandou uma foto e me disse “Suzi, isso é tanto que ainda posso dividir com mais duas famílias”. E ela fez. Teve uma mulher que recebeu os 300 reais que mandamos para ela, ela tirou R$ 25,00 disso e doou de volta para o nosso fundo, porque ela queria ajudar também. É lindo ver como esses atos de solidariedade ativam as pessoas, fazem as pessoas se levantarem e fazerem algo por alguém também. É muito importante cuidar da autoestima das pessoas e é assim que a solidariedade se espalha.

Nós levantamos o dinheiro muito rápido, em quatro dias. E isso nem foi uma surpresa pq sei que nossa rede é forte. Quem tá numa situação de maior segurança tá tendo de fazer alguma coisa. Não dá para você ficar só ali vendo sem fazer nada. É muito gratificante poder ajudar as pessoas. Eu recebo relatos das pessoas que recebem ajuda e isso me tranquiliza.

Agora estamos vendo de que lado as pessoas estão. Estão surgindo novas conexões entre grupos e pessoas, que vamos nos organizando pelo whatsapp, e estamos fazendo essa Rede de Apoio ao Combate do COVID 19. ”

<hr />

**No Rio de Janeiro, André de Carvalho, 48, viveu toda a sua vida na favela Morro da Formiga. Filho de trabalhadores migrantes, tornou-se organizador em 2013, o que, segundo ele, foi um divisor de águas em sua vida, por ter visto a possibilidade de trabalhar pela transformação social com as próprias mãos.**

“O Morro é um lugar de resistência, um quilombo. Tenho orgulho de ser daqui, pois isso me fez quem eu sou.
Existe um muro invisível que separa o asfalto da favela. O trabalho que faço aqui acontece por meio da criação de conexões entre esses mundos.

Nós, moradores das favelas, sabemos quais são as nossas demandas. Quando a pandemia começou, a preocupação das pessoas aqui era se conseguiriam comer ou não. Naquele momento, porém, tínhamos muitas parcerias, as pessoas mandavam dinheiro para cá, doações de alimentos e suprimentos básicos…

Outra lacuna tem sido a questão da educação. Estamos, portanto, nos concentrando em fazer as pessoas entenderem a seriedade do momento. Este é um trabalho muito difícil porque nossa gente tem muito na cabeça, e tem sido difícil para muitos entender que você não pode mais socializar como antes.

Há um déficit de educação no território das favelas e sabemos que é um projeto que permite que as pessoas sejam manipuladas por discursos como o deste presidente. Por isso espalhamos mensagens de áudio, stencil, graffiti, música, rap, funk. Todo tipo de ferramenta possível para enviar uma mensagem falando com a própria realidade das pessoas.

A solidariedade para mim é um princípio básico para vivermos de forma harmoniosa. É o oposto do caos social.

Tenho esperança de que, se há algo de bom que podemos tirar desta pandemia, é tecer novas redes de pessoas agindo juntas para o bem comum. ”

<hr />

**Conjunto da Maré é uma grande área de muitas favelas do Rio de Janeiro, com 140 mil habitantes. Timo Bartholl, originário da Alemanha, mudou-se para lá em 2008 e desde então vive e faz parte de coletivos de base. Faz parte do coletivo “Roça!” que produz agroecologia e também funciona como um espaço comunitário. Desde o início da pandemia ele se organiza com a recém-formada Frente de Mobilização da Maré.**

“Na favela, sempre que há uma emergência, há uma expansão das redes. A Frente de Mobilização da Maré é uma delas. Atuamos em três eixos: suporte econômico, comunicação e autocuidado. Aqui (em abril) não há exames para quem não está internado pelo SUS. No momento estamos fazendo um formulário online para tentar descobrir quais famílias suspeitam de casos para que possamos pelo menos fazer alguma projeção.

Temos um problema muito grande de descrença na mídia, então os comunicadores estão usando as redes sociais para falar com os moradores, adaptando as informações à realidade da favela. Por exemplo, aqui tem gente que não tem água em casa. Então colocamos faixas dizendo “Se você tem água em casa, compartilhe com quem não tem”.

Vejo que as pessoas estão de olho umas nas outras. As pessoas avisam, falam: “Olha, minha vizinha não tem emprego, acho que ela precisa de comida”, esse tipo de coisa.

Muitas pessoas aqui trabalham no setor informal. Muitos, por exemplo, ganham a vida vendendo lanches e bebidas em engarrafamentos. Então, de um dia para o outro não há mais fonte de receita, porque não há mais engarrafamento. Nesse momento, o apoio entre as classes sociais se tornou ainda mais importante, e as pessoas mandaram dinheiro para cá.

Eu acho que existem tipos de solidariedade.  Existe a intra território, intra classe, e existe a transterritório, transclasse.

Uma pessoa que está lá na zona sul, em plena consciência do seu privilégio, recebendo entregas na sua casa, mas que não deixa de se preocupar com pessoas que não tem esse privilégio e procura os grupos para fazer doações.

A Pandemia traz essa associação societária. Ela pede uma solidariedade. Por exemplo, no cotidiano anterior à pandemia, a minha conexão com a cidade era pelo fluxo do trânsito. Agora, eu penso se vai ter leito no hospital para mim se eu ficar doente.

Temos que ver como que a solidariedade vai sobreviver, que vida ela vai ter ao longo da pandemia.
Porque (em abril de 2020) ela ainda está apenas no começo.

Com o passar do tempo, quero ver como será a solidariedade internacional.”

<hr />

**Helena Silvestre foi ativista por toda a vida. Ela mora na zona sul da cidade de São Paulo. Lá, entre muitos outros projetos, ela começou a escola feminista Abya Yala. Um lugar onde as mulheres se organizam localmente e constroem o que Helena chama de “feminismo  favelado”.**


“A Escola Feminista Abya Yala é um desafio permanente: construir, a partir das favelas, um feminismo que expresse e abraça o que somos, com nossa trajetória de diásporas negras, genocídios indígenas, expulsão da terra e reconstrução de comunidades em territórios violados como favelas e periferias.

Como estamos enraizados em territórios que nunca existiram sem fome, nosso primeiro pensamento quando a pandemia se abateu foi: as mulheres não poderão ficar em casa enquanto suas famílias estão passando necessidade. Imediatamente iniciamos um processo de organização entre nós, tarefas compartilhadas, mapeamento de mulheres em situações extremas, mapeamento de mulheres voluntárias com um carro e mapeamento de doadores de alimentos. A partir dessas primeiras entregas, acendeu em nós o alerta da saúde mental: muitas mulheres agradecidas recebiam cestas e nos contavam, por vezes chorando, a história de como vinham já por um fio. Começamos então a organizar atendimento psicológico à distância com duas psicólogas que fazem parte da Abya Yala, construindo protocolos e procedimentos. O atendimento psicológico reforçou outro debate, que é a situação de violência doméstica com mulheres presas com seus agressores. Portanto, esta foi mais uma frente estabelecida com algumas ideias que estamos tentando produzir como redes mínimas de apoio às mulheres que sofrem assim.

Nossa adaptação foi difícil. Mas o que me inspira é ouvir de mulheres totalmente vulneráveis ​​que querem ajudar de alguma forma. Como o número de mulheres atendidas está crescendo muito, várias dizem que não sabiam da existência de grupos como o nosso, e dizem que querem fazer parte dele, costurando máscaras com tecidos doados que a gente leva, e ajudando no que pode.

Acho que com essa pandemia nos damos também conta de que os privilégios são sempre uma relação dinâmica e se nós, enquanto faveladas, não temos privilégio algum em relação às mulheres das classes médias brancas e universitárias, aos termos uma casa e trabalho assegurado estamos em situação de privilégio em relação a outras irmãs que precisam de nossa atuação.

A solidariedade é uma ação e não um discurso, é uma construção enraizada e quotidiana.

O que mais eu gosto de pensar é que, ao partir das mulheres e nossa construção enraizada e cotidiana, todo o trabalho e toda a presença será reconhecida como política, como transformadora, como ativista, como revolucionária. Isso permite eliminar a miopia que só vê política nos “atos extraordinários do herói”, pensando em outras formas de política visceralmente mais radicalizadas e de ruptura total com o capital e com a lógica ocidental. Permite também que quem quer mudar o mundo, se mude a si mesmo, porque não são apenas os atos extraordinários aqueles que contém política, mas também quem somos a todo momento conta para saber quem somos.”

<hr />

**Na aldeia Jaraguá, zona norte da cidade de São Paulo, território indígena Guarani, a família de Thiago Karai Djekupe luta pelo direito de permanecer na terra há gerações e para proteger a natureza nativa que a circunda.**

“Estávamos ocupando um terreno próximo ao nosso território para defendê-lo de investimentos imobiliários quando começou a pandemia. Voltamos então para nossa comunidade e começamos a trabalhar na prevenção imediatamente.

A primeira coisa que precisávamos fazer era nos isolar do mundo exterior. Como estamos em São Paulo, o isolamento foi muito difícil, pois aqui dependemos da economia dos Juruá (povo branco) para sobreviver.
A Covid-19 chegou então ao nosso território. Hoje (abril, 2020) temos dois casos confirmados e outros suspeitos.
Estamos exigindo testes rápidos para todos na comunidade. Até agora, tivemos algumas perdas de povos indígenas para a Covid-19 em outros territórios por todo o Brasil. Fazemos parte da Comissão Guarani Yvyrupa, que abrange o povo Guarani de todos os territórios, e da APIB, que é a Articulação dos Povos Indígenas do Brasil. Por meio dessas organizações, trocamos informações e nos organizamos para trabalhar na prevenção e na denúncia de abusos.

Já dissemos isso há muito tempo. Que se a humanidade continuar tratando a mãe Terra da maneira que tem feito, a mãe Terra tentará se livrar dela. Nós sabíamos que isso aconteceria.

Acho difícil acreditar que os Juruás vão realmente entender que é preciso mudar seus hábitos. Que é preciso respeitar e conhecer os idosos, que é preciso ser sábio, viver solidariamente, respeitar os jovens ... Acho que os Juruás esquecem muito rápido.

No modelo de civilização capitalista, é difícil aprender a viver em equilíbrio.
Mesmo depois de sofrer tanto, quando as coisas melhoram, as pessoas esquecem. As pessoas têm estado com tanta ansiedade que o seu modo de vida lhes impõe que se arriscam, porque sem arriscar as suas vidas não podem sobreviver nesse sistema.

As pessoas ainda estão confusas sobre quem são. Ailton Krenak, meu padrinho, líder indígena e autor, escreveu sobre esta lição que aprendemos com os mais velhos: adiar o fim do mundo vai levar as pessoas a reaprender a viver. Compreender essa simplicidade é o que importa. Que ter abrigo, comida, alegria e comunidade é o que precisamos para termos uma vida real e sustentável. Essa vida é possível, mas o Capital luta contra essa ideia e esse modelo de vida desde o seu início. O capitalismo está tentando destruir nossa cultura indígena como outra forma de destruir este modelo de vida comunitária e sustentável. ”

Movimento que vem subvertendo a distribuição colonial de terras no Brasil há 35 anos, e um dos maiores movimentos sociais da América Latina, o Movimento dos Sem Terra, o MST (Movimento dos Trabalhadores sem Terra) atua por meio da ocupação de vastas áreas de terra, colocando-a na produção agrícola, enquanto exige a reforma agrária. Durante a pandemia, eles têm estado muito ativos na distribuição dessa produção à quem precisa. Maria Alves é agricultora e educadora do MST, atuante ao longo de seus 66 anos. Ela contou como esse movimento trouxe a ela e a muitos outros a dignidade de serem trabalhadores de campo e trabalhar pela abundância de vida no planeta.

“Desde a primeira vez que vi uma marcha do MST sabia onde queria estar. Valorizamos a mãe terra e tudo o que ela oferece. Trabalho no setor produtivo aqui na Comuna da Terra Irmã Alberta, ocupação a apenas 30 Km da cidade de São Paulo.

O povo brasileiro come muito mal. É uma pena que nosso país aplique tantos pesticidas na produção de alimentos. Insistimos na importância de uma alimentação saudável e diversificada nas três refeições diárias. E agora com a pandemia a gente sabe que muitas pessoas não vão ter recursos para isso. Isto é uma vergonha. Um país de terras férteis, com tantos recursos naturais, não deveria deixar ninguém passando fome.

O MST está fazendo doações para quem precisa. Nós fazemos o que podemos. As pessoas contribuem com algum dinheiro. Esses valores passam a ser compras dos agricultores do MST e a produção é distribuída. Fiquei com os olhos cheios de lágrimas ao ver o quanto as famílias estavam passando necessidade e o quanto fomos capazes de ajudar. Isso está acontecendo em todo o Brasil, nas várias áreas onde temos produção de alimentos, estamos distribuindo de forma muito organizada. Nós não estamos sozinhos. Vemos muitas outras redes e movimentos fazendo isso. Isso é muito importante.

Acho que é mais um acontecimento, mais um momento que nos ajuda a refletir profundamente sobre tudo. A democratização da terra deve ser no campo e na cidade, para que todos tenham acesso às riquezas deste país. Devemos estar juntos com movimentos indígenas, quilombolas, movimentos urbanos. Todos aqueles que estão defendendo a vida em sua diversidade.

As pessoas vão começar a refletir muito sobre a solidariedade. Vão começar a pensar: “quem está do nosso lado? Quem temos que defender? Quem são nossos inimigos? ”

Acho que no final isso tudo pode ter um efeito de despertar nas pessoas.

Para fortalecer as pessoas, para que se reconheçam e tenham orgulho de quem são.

Temos que fazer um trabalho que parecia não sermos capazes de fazer até que as pessoas vissem esse mal em todo o mundo. Eu me sinto muito assim ... quero que essa pandemia acabe o mais rápido possível para que possamos retomar todo o trabalho que temos que fazer.

E tem que ser coletivo, participativo e cooperativo ”.
