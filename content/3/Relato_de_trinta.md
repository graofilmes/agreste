---
order: 9
title: Relato de Trinta
author: Ciça Coutinho
img: 3/bgs/Vinganca.jpg
alt: Fotografia de um prédio deadente na cidade
---
<div class="videoWrapper"><iframe class="full--width video--wrapper" style="object-fit: contain" src="https://player.vimeo.com/video/502312519" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>

<div class="quote"><q>A “inocuidade” do sexismo sustenta uma cultura de seres vivos transformados em mercadoria, seres que não são suficientemente bons, pois simplesmente são usados para servir aos caprichos da classe dominante, sendo vestidos/expostos/mutilados.</q><vue-tooltip text="ADAMS, Carol J.- A política sexual da carne a relação entre carnivorismo e a dominância masculina (1990)"index="1"></vue-tooltip></div>

São Paulo, carnaval de 2012. MEU aniversário de 30.  Tava gostosinho, bloco de rua, poucas pessoas, gente nossa, música ao vivo, de qualidade; xixis democráticos, mulheres fazendo cabanas pros homens, homens “cabanando” mulheres. Numa dessas voltas do banheiro/rua, um estupro.

Cultura do estupro: ambiente que banaliza e justifica a violência contra a mulher, disseminado pelas instituições tradicionais: família, igreja, escola.  E alimentado/endossado pelas mídias: cenas de novelas, comerciais de cerveja, revistas femininas, etc.

A cultura do estupro é responsável pela disseminação de ódio de gênero, sobreposição de forças, subjulgação da mulher por homens, sendo estes familiares e/ou cônjuges; pela invasão aos corpos em trânsito, o ir e vir das mulheres é totalmeste cerceado, homens nos medem da cabeça aos pés, mexem com a gente nos proferindo palavras, das calçadas, dos carros, das motos.
Este trânsito dirige a maneira como circulamos. Atravessamos ruas para minimizar o mal estar, temos medo se é tarde e estamos sozinhas.

O Brasil estampa a marca do quinto país que mais mata mulheres no mundo, crime conhecido como feminicídio. Ficando atrás de países como: Rússia- quarto, Guatemala- terceiro, Colômbia- segundo e El Salvador- primeiro; detalhe para os 4 países na América Latina. Não vou aprofundar aqui, porém diz de nossa história latinoamericana ser quatro países neste território.

O corpo da mulher é cultural e socialmente um campo de batalha. Atravessado pelo assédio, este corpo é obrigado a criar estratégias para sobreviver no universo machista.

Partindo desse contexto, o corpo enquanto casa é território da identidade, das subjetividades de cada mulher e das histórias que o compõe. Corpo casa, corpo território, corpo história é o que fez surgir a performance territórioMEU, com o pronome em maiúsculo denotando a importância da apropriação dos próprios corpos, e tendo substantivo e pronome colados fazendo associação ao corpo casa mais o pronome 'MEU' em caixa alta reatualizando o grito de liberdade. 'territórioMEU' é uma instalação performática, em que o assédio é o tema principal. A palavra assédio, que significa “perseguição com insistência”, pode ser relacionada com a palavra violência: “constrangimento físico ou moral, o uso da força, da coação”. Logo, o assédio seria o caráter constrangedor, tanto físico como moral, da violência. As questões sobre a mulher e seu corpo abordadas na performance têm como objetivo tratar de temas presentes em nosso cotidiano, como o assédio e o feminicídio. Todos os dias somos atingidos por notícias de agressões e violência contra a mulher. Nos jornais de tv e impresso, nos portais de notícias da web e em estudos acadêmicos são relatados esse problema enfrentado pela sociedade em diversos espaços e níveis, diariamente. A ideia do uso da palavra território é a de que cada corpo pertence a si, é território próprio, território de histórias, território de acontecimentos e pertencimentos. É assunto tabu que precisa ser debatido em diversas esferas: de classe, raça e gênero, a pensar numa prática antimachista. Sensibilizar o assunto para desconstruir hábitos.

Partindo da minha narrativa enquanto mulher negra, é importante salientar que mais de 50% das mulheres vítimas de assédios e feminicídios são mulheres negras. Na historiografia colonial da negritude somos exploradas sexualmente por sinhôs e sinhás, somos o corpo forte que “aguenta tudo”, “a cor do pecado”, a serviçal nas novelas, o corpo fetichizado, no caso dos homens o motorista, segurança, o “do pau grande”. Mais uma vez reforçados pela cultura do estupro hegemônica, heteronormativa e branca.

territórioMeu é uma performance instalação em que a artista pesquisadora está inserida há 5 anos. Tendo o próprio corpo como narrativa parte de seu contexto para trazer à tona a discussão sobre assédio. A pesquisa foi inciada por suas anDanças nas ruas e os atravessamentos constantes que a artista sofre. Desenvolveu dramaturgia que envolve parar e conversar pedagogicamente com o agressor; para tanto usa de sua experiência com docência em educação infantil para desenhar o texto e tentar construir um diálogo com o agressor.

A partir da experiência do solo está criando a metodologia chamada  'Dança Intuitiva' que visa mexer nas memórias corporais e dar colo, tendo em vista que os atravessamentos de violência deixam marcas físicas.
Uma pincelada desta experiência:

Aqui alguns dados históricos a respeito das mulheres que acredito ser importante contextualizar:
* 1890 - lei de defloramento: homens tinham propriedade do corpo da mulher, podendo assassiná-las caso as considerassem adúlteras e ou não virgens. Quem fazia a perícia era médicos homens.
* 1916 - O Código Civil regulamenta a condição feminina: a anulação do matrimônio, por exemplo, poderia acontecer se o marido casasse com uma mulher deflorada (não virgem) sem saber disso.
* 1932 - conquista do voto feminino no BR.
* 1967 - criado o curso de economia doméstica, que consistia em educar mulheres para os serviços domésticos, na ESALQ USP Piracicaba. O curso existiu até 1991.
* 1979 - Eunice Michelis, primeira mulher a ocupar cadeira no Senado brasileiro. Sua trajetória na política foi marcada por uma tentativa de retirar da legislação brasileira dispositivos que minavam a liberdade feminina. Foi a parlamentar que reivindicou a queda da lei de 1890. A proposição chegou a ser aprovada pela CCJ do Senado. Mas, apesar disso, foi arquivada cinco anos depois e a previsão só deixou de existir, de fato, após a entrada em vigor do Código Civil de 2002 - mesmo com a equiparação entre homens e mulheres prevista pela Constituição Federal de 1988.
* 1988 - constituição que igualou os direitos dos homens e das mulheres
* 2006 - criação da lei Maria da Penha, 11.340
* 2020 - lei Maria da Penha faz 14 anos dia 7 de agosto e São Paulo fecha primeiro semestre com maior número de feminicídios desde a criação mesma.

Neste momento pandêmico a violência contra a mulher aumentou 50% tendo em vista sobretudo que o agressor quase sempre mora com a vítima. Além da atenção ao covid, a questão da violência de gênero passou a ser levantada por órgãos públicos e privados, e ainda engatinham na necessidade urgente de combate à violência de gênero. Tendo em vista que a construção da cultura do estupro enraizada ainda corrobora com o silêncio e medo que as mulheres têm de denunciar seus companheiros.

Assombramos marcas infelizes de um assédio por segundo, um estupro a cada onze minutos e uma morte a cada duas horas. Dados da Federação Brasileira de Segurança Pública e UNIFESP.
Falar sobre a assédio e violência doméstica nunca foi tão urgente. São doenças sociais que merecem nosso olhar cuidadoso e carinhoso para acolher mulheres e homens. Num trabalho de formiguinha que se cada casa fizer seu papel, acessamos mais pessoas tornando o debate acessível e construtivo.

O estupro me marcou pra sempre. Todas as vezes que eu descrevo esta situação eu volto na sensação. Porém, faço questão de seguir contando e descrevendo os instantes desgraçados que vivi, vivo e viverei. Hábitos enraizados precisam de sensibilização, desconstrução para reconstruir novas possibilidades de narrativas de feminines e das nossas relações entre gêneros.

No evento do estupro que vivi, meu irmão de sangue respondeu a violência com violência. Foi a reação que lhe coube ao me ver invadida; estávamos celebrando a vida e meus 30, já era altas da madrugada e vivíamos instantes de felicidade. O bloco estava tão pacífico que outro homem empurrou meu irmão pro chão tentando acabar com a briga. Meu irmão contou o motivo, o cara o tirou do chão e eles bateram juntos no estuprador. A surra lhe rendeu um dedo quebrado. Fomos para o hospital.

No hospital os profissionais sabem que aquele dedo quebrado quase sempre diz respeito a uma briga, e a enfermeira que o atendeu perguntou o motivo. Ele contou. Ela o agradeceu e parabenizou por ele me defender, e contou a história a seguir – antes de começar vou criar um nome fictício para a enfermeira, será Ana.

As enfermeiras mulheres criaram entre elas um objeto que as protegesse quando saem dos plantões noturnos e se sentem expostas e vulneráveis, de modo que elas escolheram a cola super bonder como aliada. Ela ressaltou que quando escolheram a “arma” não o fizeram para usar, mas para ter um álibi na bolsa. Eis que um dia, Ana e uma companheira de trabalho saem de um plantão à tarde, pegam o ônibus e ambas sentam nas cadeiras do corredor de modo a ficarem mais ou menos de frente pra outra para seguirem o caminho conversando.

Ana começou a sentir algo em contato com suas costas, e pensou que pudesse ser a questão da disputa de espaço, algo bastante comum quando mulheres e homens sentam lado a lado. Sim, era um homem ao lado dela. E estava com a perna mais aberta do que é delimitado. O que parecia ser uma encostada virou movimento, e ela estranhou. Olhou para o lado e o cara estava masturbando nela. Ela ficou estarrecida e em choque e naquele instante lembrou da cola. Se voltou pra sua bolsa, pegou, despejou toda no ato, e anunciou para a população transeunte o que estava acontecendo.

O motorista e o cobrador ofereceram ir para delegacia. Pararam o ônibus no ponto seguinte e disse que encerrava a corrida ali por motivos óbvios e que quem não quisesse descer poderia ir como testemunha. Ninguém desceu do ônibus.

Aquela altura, o cara já tinha tirado seu paletó e coberto a cena do crime. Ao chegar na delegacia precisou ligar para alguém socorre-lo, ligou para a esposa. E na sequência foram para o hospital.

Bem. Ana é da área da saúde e costumo dizer que cada área tem uma “rádio” local, dessas que vai passando informações no boca a boca. A das enfermeiras não é diferente. Então elas tiveram notícias do ocorrido. O cara precisou passar por uma intervenção cirúrgica, e no momento da sedação, médicos tiveram que decidir qual das partes seria recuperada, mais que isso, preservada uma vez que dependia do tecido a ser separado.

No momento o que foi possível foi salvar a mão. Esse pau nunca mais fará uma próxima vítima.

# Leia mais em:

* <a href="https://www.gazetadopovo.com.br/vida-e-cidadania/a-legislacao-do-defloramento-3h52bbgsvgdzy3fvajzcwo5se/">https://www.gazetadopovo.com.br/vida-e-cidadania/a-legislacao-do-defloramento-3h52bbgsvgdzy3fvajzcwo5se/</a>
* <a href="https://www.esalq.usp.br/institucional/linha-do-tempo">https://www.esalq.usp.br/institucional/linha-do-tempo</a>
* <a href="https://migalhas.uol.com.br/quentes/275461/passado-e-presente-conheca-as-leis-sobre-direito-das-mulheres-no-brasil">https://migalhas.uol.com.br/quentes/275461/passado-e-presente-conheca-as-leis-sobre-direito-das-mulheres-no-brasil</a>
* ADAMS, Carol J. _A política sexual da carne a relação entre carnivorismo e a dominância masculina_ (1990)
* BRAGA, Amanda. _A história da beleza negra no Brasil discursos, corpos e práticas_ (2015)
* CAULFIELD, Sueann. _Em Defesa da Honra_ (2000)
