---
order: 4
title: Outro mundo é cantado
author: Fanfarra Clandestina
img: 3/bgs/Sem-titulo1.jpg
alt: Fotografia de um prédio deadente na cidade
---

<div class="quote"><q>Matar os sonhos só poderia nos levar à pesadelos.</q><br />
David Graeber</div>
<hr />
Playlist da Fanfarra Clandestina:<br />
<iframe class="playlist" src="https://www.youtube.com/embed/videoseries?list=PLboUrO1KCPs3c_NQW-8JYfK_ZtbsMOa-I" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<hr />
O som de um saxofone entoa a melodia de “El derecho de vivir en paz”, de Victor Jara, em meio a uma barricada em pleno confronto com a polícia durante os protestos pela constituinte no Chile em 2019.

Na França, trombones, trompetes e trombonitos fazem uma multidão se unir em êxtase ao som de “Du mouvement Social”, tocados pela Fanfarre Invisible durante um ato do movimento dos coletes-amarelos na França, em 2018.

No Brasil, a manifestação do Movimento Passe Livre em em 2013, em São Paulo, toma unidade entoando palavras de ordem mescladas ao som de “Banditismo por uma questão de classe”, da Nação Zumbi, tocados pelos sopros e tambores da Fanfarra do M.A.L.

Estas e outras cenas compiladas na playlist que acompanha este artigo. São momentos em que a música foi um elemento a mais catalisador para ativar o senso de possibilidades, de empoderamento da multidão, do movimento emancipatório.

O antropólogo David Graeber escreveu sobre imaginação política em seu livro Direct Action - an ethnography, obra produzida em seu tempo como ativista e pesquisador na  Rede de Ação Direta (em inglês, DAN - Direct Action Network), grupo de ativistas que agiu para, e por um momento conseguiu, barrar a reunião da Organização Mundial do Comércio em Seattle, em 1999.<vue-tooltip text="GRAEBER, David.  _Direct Action - An Ethnography AK Press_ (2009)" index="1"></vue-tooltip> Os acontecimentos em Seattle marcaram um momento fundamental do movimento Altermundialista, que se contrapôs a uma ordem de mundo neoliberal, mas, como Graeber explica em seu livro, o movimento não começou ali. O antropólogo explica que o movimento Zapatista, que teve início em 1994 no México, foi o marco de uma virada na narrativa global de esquerda, especialmente por suas características de rejeição de modelos pré concebidos de revolução e um chamado à formação de comunidades autônomas e à criação hoje do mundo que se quer viver em movimento. Foi a partir dos encuentros Zapatistas que surgiu a Ação Global dos Povos, AGP, que nas palavras de Graber, “...tinha como um de seus objetivos colocar a ação direta não violenta de volta ao palco global como uma força para a revolução global.” (GRAEBER, 2009) Dos Zapatistas à Seattle, de Seattle às ruas de São Paulo, o movimento por justiça global globalizou um repertório que alimentou e deu forças à palavras de ordem que ecoaram e ecoam até hoje, como uma grande rede global de trocas. Foi em Seattle que surgiu um grupo chamado Infernal Noise. Primeiro uma bateria de protesto extremamente potente que utilizava-se também dos sons de sirenes e ruídos para apoiar as marchas pelas ruas, e que com o tempo teve instrumentos de sopro integrados ao seu som, se tornando uma fanfarra que adicionava uma camada a mais simbólica marcando uma época e um sentimento coletivo.
Integrantes da Infernal Noise certa vez, vieram à São Paulo participar de um festival punk chamado Verdurada. Ali, passaram seu repertório de músicas e táticas de rua para alguns participantes do festival, que passaram para outros e que foi adotada pela Fanfarra do M.A.L., grupo de sopros e percussão que se dedicava a tocar nos atos do movimento autônomo em São Paulo, tendo grande participação no Movimento Passe Livre.

Músicas que são hoje tocadas pela Fanfarra Clandestina, um grupo que atua também nos protestos de rua dos movimentos autônomos em São Paulo e em espaços de resistência como ocupações e festivais.

Bella Chao, A las Barricadas, A Internacional, Ou est la liberté, Du mouvement Social, We shall overcome, Which side are you on, Eu quero é botar meu bloco na rua, Banditismo por uma questão de classe, Clandestino e outras são algumas das músicas compiladas aqui e compartilhadas ao redor do globo, sendo atualizadas e ressignificadas em seus contextos,  inspirando momentos de suspensão dos aparatos burocráticos e fortalecendo situações de insurreição. A imaginação política à que Graeber se refere floresce nesses momentos que “parecem ter o efeito de escancarar os horizontes das possibilidades” em que “as pessoas se sentem no direito e, mais que isso, sentem a necessidade de recriar e re-imaginar tudo ao seu redor.”  (2009, p. 530)

Ou, como disse o jazzista Duke Ellington, “Isso não é piano, isso é sonhar”<vue-tooltip text="<a href='https://kenburns.com/films/jazz-2/' target='_blanc'>BURNS, Ken - _“Documentary minisseries - Jazz”_ - (2001)</a>" index="2"></vue-tooltip>