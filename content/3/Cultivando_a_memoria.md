---
order: 8
title: Cultivando a memória do audiovisual alagoano
author: Larissa Lisboa
description: 'Revisão: Felipe Benício e Rose Monteiro'
img: 3/bgs/Bonde-das-Maravilhas.jpg
alt: Fotografia de um prédio deadente na cidade
---

<foto src="3/livro.jpg" alt="Livro: Panorama do Cinema Alagoano" width="100%"></foto>

A existência do livro Panorama do Cinema Alagoano (1983), de Elinaldo Barros, tem ainda mais impacto hoje em mim, pois é um registro, obra, incentivo que ainda segue escasso em Alagoas e em tantos outros lugares. Seja em sua primeira edição, de 1983, ou na segunda, de 2010, registra os conhecimentos colhidos/vividos por Elinaldo junto à produção de cinema alagoana.

Panorama apresenta um mapeamento da produção cinematográfica de 1921 a 1983, fruto da vivência de Elinaldo junto ao Festival de Cinema de Penedo, tendo como principal estímulo e fonte a cartilha Subsídios à História da Cinematografia em Alagoas (1974), de José Maria Tenório Rocha, aliado também à pesquisa em jornais. Em sua segunda edição, o livro recebeu um breve capítulo que apresenta os anos 2000 (Chegando ao Século XXI), apontando com brevidade a presença de realizadores que surgiram após a sua publicação primeira.

A viabilização desse livro em 1983, por meio do Departamento de Assuntos Culturais da Secretaria de Educação e Cultura, foi reforçada pelo expresso agradecimento nas páginas iniciais “agradecimento aos professores Ernani Mero (ex-diretor do DAC) e José Moacir Teófilo (ex-secretário da Educação e da Cultura)”, fato que também possibilitou a chegada dessa obra a acervos como o da Universidade Federal de Alagoas (UFAL), onde o encontrei 20 anos após a sua publicação.

Já a publicação de 2010 foi fruto da articulação da Associação Brasileira de Documentaristas e Curta-metragistas de Alagoas (ABDeC-AL), que viabilizou a ponte entre a Universidade Federal de Alagoas (UFAL) e o Centro Universitário CESMAC, com o objetivo de realizar a publicação da segunda edição revista e ampliada pela EDUFAL.

Panorama do Cinema Alagoano (1983, 2010) é fonte para estudo, preservação de memória, registro histórico. Em 99 anos de existência do cinema alagoano, esse é o único livro publicado que proporciona um mergulho na história do cinema e da produção audiovisual do estado, fato que já indica a carência de incentivo à cultura e à preservação artístico-cultural, como também um dos símbolos do descaso  e desvalorização do registro da memória alagoana.

# Panorama é semente

O encontro com Panorama me marcou, mas antes de encontrar o livro, soube da existência de Elinaldo Barros, professor e crítico de cinema, e fui atrás dele. Encontrei em Panorama um grito entalado de uma cultura persistente, de pessoas que encontraram na arte uma forma de expressão e dispuseram de recursos próprios para construir seus filmes em Super 8 ou outras formas.

Grito que ainda ecoa nos dias de hoje, assim como ecoava em 2007, quando comecei a dar meus primeiros passos como pesquisadora e realizadora audiovisual, momento em que vi seu eco ganhar bocas que se dispunham a afirmar e reafirmar que havia produção artística em Alagoas, e que, mesmo sem financiamento público, formação ou fácil acesso a equipamentos, filmes eram produzidos.

Grito que se transformou, para mim, em semente, graças à Nataska Conrado, que me fez a pergunta: “Por que você não faz um catálogo?”, momento que me guiou a ousar propor um catálogo, proveniente do desejo de ver uma atualização de Panorama do Cinema Alagoano como meu Trabalho de Conclusão de Curso (TCC) de Jornalismo, que resultou na catalogação de mais de 200 filmes alagoanos, dando origem ao Catálogo da Produção Audiovisual Alagoana (2008). Mal sabia que estava germinando ali um trabalho que daria mais frutos do que poderia imaginar, e que o livro que propus em meu TCC não seria publicado/impresso por falta de recursos, mas que poderia ser transformado em um espaço virtual democrático que compartilhamos há cinco anos como Alagoar (www.alagoar.com.br).

Além de serem fontes bibliográficas para pesquisa, os livros possibilitam a aproximação das pessoas com conhecimentos, universos, referências. Passei a ter uma outra relação com Panorama quando comecei a dar cursos e oficinas em 2016. Compreendi também que muitas pessoas nem saberiam da existência do livro de Elinaldo se não o tivesse apresentado como referência daquele curso/oficina ou como base para o Alagoar.

Passei a me angustiar por não parecer possível que houvesse uma nova edição de Panorama, ou por desconhecer se era possível providenciar mais cópias da 2ª edição, de 2010. Germinou em mim o desejo de impulsionar isso, mas o que pude fazer foi falar em oportunidades como esta.

Em 2018, a convite do Fórum Setorial do Audiovisual Alagoano (FSAL), o Alagoar colaborou na realização de um catálogo construído com o objetivo de apresentar o audiovisual alagoano no 51º Festival de Brasília do Cinema Brasileiro, Cinema Sururu - O novo audiovisual produzido em Alagoas (2018), que teve um quantidade limitada de exemplares e foi lançado em Brasília-DF na programação do Festival.

<foto src="3/sururu.jpg" alt="Catálogo da Mostra Sururu de Cinema" width="100%"></foto>

A pesquisa e produção de conteúdo foi feita por Amanda Duarte, Leonardo Amaral, Janderson Felipe e por mim, a partir da referência de obras que participaram da Mostra Sururu de Cinema Alagoano (2009 a 2017), propondo um recorte com mais ou menos cinquenta obras. A curadoria e coleta de informações dos filmes participantes da Mostra foi realizada em quatro dias. Cinema Sururu contou também com espaço para apresentar obras em desenvolvimento contempladas por editais públicos em Alagoas.

Longe das condições ideais, resultado de trabalho voluntário e sendo ou não conhecido em Alagoas, ou mesmo reconhecido como uma publicação que deva ser usada como referência, este foi/é o primeiro catálogo sobre o cinema alagoano que foi lançado.

Em 2019, como celebração dos 10 anos da Mostra Sururu de Cinema Alagoano, o FSAL construiu o Catálogo Comemorativo - 10 anos da Mostra (2019), tendo Maysa Reis e Rafhael Barbosa como responsáveis pelo conteúdo e pesquisa, partindo do levantamento realizado para o Cinema Sururu (2018). O Catálogo Comemorativo foi lançado no encerramento da 10ª Mostra Sururu de Cinema Alagoano.

Seja em formato de catálogo ou em outro formato, a realização de publicações impressas ou virtuais sobre o audiovisual alagoano proporciona reflexões, diálogos, críticas, referências, além de ser um investimento no registro e preservação da memória da cultura alagoana. Semeio também o desejo de ver surgir mais publicações sobre a Mostra Sururu de Cinema Alagoano, sobre a crítica do cinema alagoano, sobre o que é fazer audiovisual/cinema em Alagoas.

# A janela do audiovisual alagoano

O Catálogo da Produção Audiovisual Alagoana foi encaminhado como projeto de livro para editais, não obteve aprovação, ficou estagnado e ainda parece inviável. Como desenvolver um detalhado mapeamento de filmes alagoanos (de 1921 até os dias atuais) e articular a publicação sem recursos? Como forma de superar essa limitação, comecei a pagar hospedagem e domínio em 2010, no intuito de dar forma ao catálogo em um site.

Em 2015, o espaço virtual que consegui construir sozinha estava desatualizado/abandonado. Amanda Duarte reconheceu o potencial do que tentei esboçar  e propôs atuar como parceira do site/projeto para que pudéssemos lançá-lo em março daquele ano. Foi transformador conseguir ver o site tomar forma como espaço de diálogo, memória e preservação do audiovisual alagoano. A parceria com Amanda me ensinou sobre gestão, comunicação, manutenção, me fez perceber que desejava seguir pesquisando sobre a produção audiovisual alagoana e que poderia desenvolver isso, na medida do possível, junto ao Alagoar.

A existência do Alagoar como janela do audiovisual alagoano parte da inspiração em pessoas que realizam obras com ou sem recurso. O site foi e é viabilizado através de pessoas que colaboram de forma voluntária. Através dessa iniciativa foi/é possível democratizar o acesso à informações sobre a produção audiovisual alagoana e brasileira, como também estimular a preservação da memória da cultura alagoana.

Atualmente é possível acessar <a href="https://alagoar.com.br/tag/filme-alagoano/" target="_blanc">mais de 100 textos/críticas sobre filmes alagoanos</a>, sendo a maior parte sobre obras exibidas na Mostra Sururu de Cinema Alagoano entre 2016 e 2019. O incentivo para essa produção escrita tem como fonte a realização de edições do <a href="https://alagoar.com.br/tag/laboratorio-de-critica-cinematografica/" target="_blanc">Laboratório de Crítica Cinematográfica</a> pelo Sesc Alagoas, pelo Mirante Cineclube e pela Mostra Sururu.

Muitos espaços no Alagoar surgiram através de proposições de colaboradores, entre eles, entrevistas, curadorias, coberturas e críticas. O <a href="https://alagoar.com.br/cadastre/" target="_blanc">cadastro</a> aberto para profissionais, obras audiovisuais, produtoras, cineclubes e projetos são convites para que os interessados se apropriem desses e de outros espaços do site.

Estava nos planos da celebração dos 5 anos do Alagoar propor mais espaços para diálogos, a princípio, presenciais, nem se cogitava que era possível realizar encontros virtuais. Em meio ao isolamento social devido à pandemia de COVID-19, a celebração foi transformada numa entrevista transmitida pelo Instagram (@alagoar) sobre a existência do Alagoar, com Pedro Krull e Larissa Lisboa.

<foto src="3/instagram.jpg" alt="printo do instagram @alagoar" width="20%"></foto>

Em cinco meses, foram realizados vários bate-papos e trocas de experiências através de lives. Além do deleite com o encontro entre os trabalhadores do audiovisual alagoano, por conta dessa iniciativa e por meio desse recurso, colhe-se também como resultado o registro e preservação de experiências, conhecimentos e memórias audiovisuais alagoanas.

Graças à colaboração de inúmeros trabalhadores e trabalhadoras do audiovisual alagoano, compartilhamos diálogos sobre direção de fotografia, direção de arte, still e making of, pós-produção, desenho de som, animação, trilha sonora, roteiro, direção, cineclubismo, processos de realização, videodança, protagonismo da mulher no cinema, criação artística na quarentena e tradução (audiovisual acessível e língua estrangeira). Também foram compartilhadas vivências sobre os lugares de atuação, compondo uma série de entrevistas e bate-papos que está disponível no site e no instagram.

Além de seguir propondo diálogos através das lives, também foi possível identificar outros formatos para troca de experiência e diálogo, como o Webinário. Em agosto foi realizado o Webinário Alagoanas da Imagem, ação realizada em conjunto com a Cuidadoria do Ser, graças à colaboração das produtoras culturais Kelcy Ferreira e Rosana Dias, que abraçaram a proposta junto a mim e compartilharam seus conhecimentos sobre a atuação de mulheres nas artes visuais e no audiovisual no estado.

<foto src="3/meet.jpg" alt="Live com 14 pessoas reunídas" width="100%"></foto>

Em realização conjunta com o Mirante Cineclube, foi possível expandir os diálogos e propor conversas sobre as janelas do cinema negro brasileiro, através da série Mostra Quilombo Convida, ação que está sendo transmitida pelo <a href="http://abre.ai/cinemanegrononordeste" target="_blanc">Youtube</a> do Cineclube, como parte integrante da Mostra Quilombo de Cinema Negro.

O presente e o futuro do Alagoar estão nas colaborações, assim como a construção da memória cultural nas ações de iniciativas com e sem recursos, que afloram no conteúdo que pode ser acessado no site.

No menu do site, em “Cadastre”, há um formulário para submissão de proposta de <a href="https://alagoar.com.br/curadoria/" target="_blanc">curadoria de filmes e videoclipes alagoanos</a>, que é um convite para todas as pessoas que desejem apresentar um mergulho em algumas das obras catalogadas. Também há fichas para cadastro de obras audiovisuais alagoanas, cineclubes, profissionais e produtoras que atuam no estado. Há uma chamada aberta por tempo ilimitado para colaborar escrevendo sobre filmes alagoanos e/ou sobre o cinema brasileiro. O contato pode ser pelo e-mail audiovisualagoas@gmail.com ou pela redes do @alagoar (Instagram e Facebook).

Em recente diálogo com Laís Araújo, para construção da matéria “<a href="https://alagoar.com.br/pensamentos-de-quem-guarda-a-memoria-conheca-larissa-lisboa/" target="_blanc">Pensamentos de quem guarda a memória: conheça Larissa Lisboa</a>” para o Alagoar, vivenciei com mais intensidade a experiência de refletir sobre a minha relação com o audiovisual alagoano, a partir do trabalho que desenvolvo, admiração que me impulsiona e a paixão pela memória e pelo cinema.