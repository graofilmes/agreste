export const state = () => ({
  currentMenu: [],
  editions: [
    {name: 'Cidade', year: '2017'},
    {name: 'Resistência', year: '2015'}
  ]
})

export const mutations = {
  updateMenu(state, content) {
    state.currentMenu = content
  }
}
